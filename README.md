# Springbok

A bot for handling agile project tasks in various communication tools. 


## Building

To build the entire workspace run:

```shell
cargo build --workspace
```

To check the code quality the following tools are supported:

Format:

```shell
cargo fmt -- --check
```

Clippy:

```shell
cargo clippy -- -W clippy::pedantic -D warnings
```

## Testing

To test the entire workspace use the following:

```shell
cargo test --workspace --verbose
```

